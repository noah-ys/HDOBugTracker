import os
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
#import from models
from .models import *

# Create your views here.
def indexView(request):
	template = loader.get_template('index.html')
	context = {}
	return HttpResponse(template.render(context, request))

def view_Ticket(request):
	userTable = User.objects.all()
	TicketTable = Ticket.objects.all()
	#used to send information to our template
	context = {
		'bugs': TicketTable,
		'user':userTable,
	}
	return render(request, 'view_Ticket.html', context)
