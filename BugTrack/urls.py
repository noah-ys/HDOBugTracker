from django.urls import path
from . import views

urlpatterns = [
	path('', views.indexView, name='home'),
	path('view_Ticket', views.view_Ticket, name='viewticket'),
]